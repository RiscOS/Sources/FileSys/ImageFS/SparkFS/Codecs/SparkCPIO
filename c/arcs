/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/Codecs/SparkCPIO/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.arcs */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "kernel.h"
#include "swis.h"

#include "SparkLib/zflex.h"

#include "Interface/SparkFS.h"

#include "SparkLib/sfs.h"
#include "SparkLib/zbuffer.h"
#include "SparkLib/sarcfs.h"
#include "SparkLib/err.h"

#include "convert.h"
#include "zfile.h"
#include "arcs.h"
#include "cat.h"


/****************************************************************************/

/* sequence should be                            */
/* 1 ensure enough memory for decompression code */
/* 2 ensure enough memory for destination        */
/* 3 ensure enough memory for source             */

/* output can be one of two things    */
/* 1 output to a fixed memory address */
/* 2 output to a filestructure        */


#define XMEM  1
#define XFILE 2


_kernel_oserror * loadfile(linkblock * linkb)
{
 _kernel_oserror * err;
 int               sfh;
 int               dfh;
 char            * srcbuffer;
 char            * destbuffer;
 int               dlen;
 int               slen;
 archive         * arc;
 int               fn;

 arc =linkb->openload.arc;
 fn  =linkb->openload.fn;
 slen=linkb->loadfile.slen;
 dlen=linkb->loadfile.dlen;
 dfh =linkb->loadfile.dfh;
 srcbuffer=linkb->loadfile.src;
 destbuffer=linkb->loadfile.dest;

 sfh=arc->fh;

 if(!dfh)
 {
  err=readblock(&linkb->loadfile.dest,sfh,arc->hdr[fn].length,
                                                  COPYCRC|COPYDECODE);
 }
 else
 {
  err=copyfile(sfh,dfh,arc->hdr[fn].length,COPYCRC|COPYDECODE);
 }

 return(err);
}



static _kernel_oserror * lineupdata(archive * arc,int fn)
{
 _kernel_oserror * err;
 int               sfh;
 int               status;
 int               type;
 heads           * ahdr;

 err=NULL;
 ahdr=&arc->hdr[fn];
 sfh=arc->fh;

 err=seek(sfh,ahdr->fp);

 if(!err) err=skipheader(arc);

 return(err);
}



_kernel_oserror * openload(linkblock * linkb)
{
 _kernel_oserror * err;
 archive         * arc;
 int               fn;



 arc=linkb->openload.arc;
 fn =linkb->openload.fn;

 err=openarc(arc,'r');
 if(!err)
 {
  err=lineupdata(arc,fn);
  if(err) closearc(arc);
 }

 linkb->openload.slen=arc->hdr[fn].size;
 linkb->openload.dlen=arc->hdr[fn].length;

 return(err);
}





_kernel_oserror * closeload(linkblock * linkb)
{
 _kernel_oserror * err;
 archive         * arc;
 int               fn;

 arc=linkb->closeload.arc;
 fn =linkb->closeload.fn;

 err=closearc(arc);

 return(err);
}



/*****************************************************************************/
/* save chunk of memory, dest is file handle to write it to */


_kernel_oserror * savefile(linkblock * linkb)
{
 _kernel_oserror * err;

 linkb=linkb;

 err=NULL;

 return(err);
}




_kernel_oserror * opensave(linkblock * linkb)
{
 _kernel_oserror * err;
 linkb=linkb;

 err=NULL;

 return(err);
}


_kernel_oserror * closesave(linkblock * linkb)
{
 _kernel_oserror * err;
 linkb=linkb;

 err=NULL;

 return(err);
}
